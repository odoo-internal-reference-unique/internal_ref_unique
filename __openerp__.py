{
    'name' : 'Internal Ref Unique',
    'version' : '0.2',
    'author' : 'Mithril Informatique',
    'sequence': 120,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'product',
    ],
    'data' : [
    ],

    'installable' : True,
    'application' : False,
}
